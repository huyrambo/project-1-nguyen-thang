const express = require('express'),
    app = express(),
    port = process.env.PORT || 4000,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const fileUpload = require('express-fileupload');
mongoose.connect('mongodb://localhost:27017/booking', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("Connected!");
}).catch((err) => {
    console.log(err);
});

// Add headers

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors({ origin: '*' }))
app.use(
    fileUpload({
        useTempFiles: true,
        tempFileDir: '/tmp/',
    })
);
var router = require('./src/routers/index');
router(app);
app.use(function (req, res) {
    res.status(404).send({ url: req.originalUrl + 'not found' });
});
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

