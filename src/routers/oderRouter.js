const express = require('express');
const {
    getAllOder,
    createOrder,
    deleteOder,
    updateOder,
    paganationOder,
    addItemOrder,
    deleteItemOrder
} = require('../controllers/oderController');



const router = express.Router()

router.route('/order')
    .get(getAllOder)
    .post(createOrder);
router.route('/order/:id')
    .delete(deleteOder)
    .put(updateOder);
router.route('/orderPagination/:activePage')
    .get(paganationOder);
router.route('/addItemOrder/:id')
    .put(addItemOrder);
router.route('/deleteItemOrder/:idOrder/:idItem')
    .put(deleteItemOrder);
module.exports = router;