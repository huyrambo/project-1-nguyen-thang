const express = require('express');
const {
    getAllProduct,
    getProductById,
    createProduct,
    deleteProduct,
    updateProduct,
    paginationProduct,
    search
} = require('../controllers/productController')
const router = express.Router();
router.route('/product')
    .get(getAllProduct)
    .post(createProduct);
router.route('/product/:id')
    .get(getProductById)
    .delete(deleteProduct)
    .put(updateProduct);
router.route('/productPagination/:activePage/:type')
    .get(paginationProduct);
router.route('/search/:name')
    .get(search)
module.exports = router;