const express = require('express');

const {
    getAllUser,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
    login,
    PaginationUser
} = require('../controllers/userController')

const router = express.Router();
router.route('/user')
    .get(getAllUser)
    .post(createUser);
router.route('/user/:id')
    .get(getUserById)
    .delete(deleteUser)
    .put(updateUser);
router.route('/login')
    .post(login);
router.route('/userPagination/:activePage')
    .get(PaginationUser)
module.exports = router;
