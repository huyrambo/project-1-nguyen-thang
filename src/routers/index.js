
const userRouter = require('./userRouter');
const productRouter = require('./productRouter');
const oderRouter = require('./oderRouter');
const fileRouter = require('./fileUpload');

module.exports = (app) => {
    app.use('/', [
        userRouter,
        productRouter,
        oderRouter,
        fileRouter
    ])
}