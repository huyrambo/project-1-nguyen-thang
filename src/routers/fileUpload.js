const express = require('express');
const {
    upload
} = require('../controllers/handlingFile');

const router = express.Router();
router.route('/upload').post(upload);

module.exports = router;
