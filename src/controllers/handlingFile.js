
exports.upload = function (req, res) {
    let path = 'frontendWebBookHotel/public/image' + "\\" + req.files.upload.name;
    console.log(req.files.upload.name);
    req.files.upload.mv(path, (err) => {
        if (!err) {
            console.log("SUCCESS");
        } else {
            console.log(err);
        }
        res.status(200).json({ url: req.files.upload.name });
    });
};
