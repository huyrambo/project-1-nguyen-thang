const Product = require('../models/product');

exports.getAllProduct = async (req, res) => {
    const pro = await Product.find();
    res.status(200).json({ ListProduct: pro });
};
exports.getProductById = async (req, res) => {
    const pro = await Product.findById(req.params.id);
    res.status(200).json({ pro });
};
exports.createProduct = async (req, res) => {
    const pro = await Product.create(req.body);
    res.status(200).json({
        status: true,
        data: pro
    });
};
exports.deleteProduct = async (req, res) => {
    const pro = await Product.findByIdAndDelete(req.params.id);

    res.status(200).json({
        status: true
    })
};
exports.updateProduct = async (req, res) => {
    const pro = await Product.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true });
    res.status(200).json({
        status: true
    })
}
exports.paginationProduct = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 9,
    }

    if (req.params.type == 1) {
        const pro1 = await Product.paginate({}, option);
        res.status(200).json({
            data: pro1
        })
    }
    else if (req.params.type == 2) {
        const pro2 = await Product.paginate(Product.find({ address: "Hà Nội" }), option);
        res.status(200).json({
            data: pro2
        })
    }
    else if (req.params.type == 3) {
        const pro3 = await Product.paginate(Product.find({ address: "Sài Gòn" }), option);
        res.status(200).json({
            data: pro3
        })
    }
    else {

        const pro4 = await Product.paginate(Product.find({ address: "Đà Nẵng" }), option);
        res.status(200).json({
            data: pro4
        })
    }
};
exports.FindHaNoi = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 9,
    }
    const pro = await Product.paginate(Product.find({ address: "Hà Nội" }), option);
    res.status(200).json({
        data: pro
    })
};
exports.FindSaiGon = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 9,
    }
    const pro = await Product.paginate(Product.find({ address: "Sài Gòn" }), option);
    res.status(200).json({
        data: pro
    })
};
exports.FindDaNang = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 9,
    }
    const pro = await Product.paginate(Product.find({ address: "Đà Nẵng" }), option);
    res.status(200).json({
        data: pro
    })
};
exports.search = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 9,
    }
    const query = { $regex: req.query.name, $options: 'i' }
    const pro = await Product.paginate(Product.find(query), option);
    res.status(200).json({
        data: pro
    })
}

