
const { findById } = require('../models/order');
const Oder = require('../models/order');
const User = require('../models/user');
exports.getAllOder = async (req, res) => {
    let data = [];
    const order = await Oder.find();
    for (let index = 0; index < order.length; index++) {
        const e = order[index];
        const user = await User.findById(e.idUser);
        let x = {
            name: user.name,
            email: user.email,
            orderRoom: e.orderRoom,
            timeOrder: e.timeOrder,
            sum: e.sum
        };
        data.push(x);

    }
    res.status(200).json(data);
};
exports.createOrder = async (req, res) => {
    let d = new Date;
    let date = d.getHours() + ':' + d.getMinutes() + '  ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
    let listData = []
    let sum = 0
    let listOrder = req.body.orderRoom
    for (let index = 0; index < listOrder.length; index++) {
        const element = listOrder[index];
        let total = element.price * element.quantity;
        let t = { ...element, totalPrice: total }
        sum += total
        listData.push(t);
    }
    console.log(listData);
    let data = {
        ...req.body,
        timeOrder: date,
        orderRoom: listData,
        sum: sum
    }
    const order = await Oder.create(data);
    res.status(200).json({
        status: true,
        data: data,
    })
};
exports.deleteOder = async (req, res) => {
    const order = await Oder.findByIdAndDelete(req.params.id);
    res.status(200).json({
        status: true
    })
};
exports.updateOder = async (req, res) => {
    let d = new Date;
    let date = d.getHours() + ':' + d.getMinutes() + '  ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
    let listData = []
    let sum = 0
    let listOrder = req.body.orderRoom
    for (let index = 0; index < listOrder.length; index++) {
        const element = listOrder[index];
        let total = element.price * element.quantity;
        let t = { ...element, totalPrice: total }
        sum += total
        listData.push(t);
    }
    console.log(listData);
    let data = {
        ...req.body,
        timeOrder: date,
        orderRoom: listData,
        sum: sum
    }
    const order = await Oder.findByIdAndUpdate({ _id: req.params.id }, data, { new: true });
    res.status(200).json({
        status: true
    })
};
exports.paganationOder = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 20,
    }
    let data = [];
    const order = await Oder.paginate({}, option);
    for (let index = 0; index < order.docs.length; index++) {
        const e = order.docs[index];
        const user = await User.findById(e.idUser);
        let x = {
            _id: e._id,
            name: user.name,
            email: user.email,
            orderRoom: e.orderRoom,
            timeOrder: e.timeOrder,
            sum: e.sum
        };
        data.push(x);
    }
    res.status(200).json({ activePage: req.params.activePage, totalPages: order.totalPages, data: data });
};
exports.addItemOrder = async (req, res) => {
    const order = await Oder.findById(req.params.id);
    let bo = req.body
    let d = new Date;
    let date = d.getHours() + ':' + d.getMinutes() + '  ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
    let listOrder = order.orderRoom
    let t = bo.price * bo.quantity
    let a = {
        price: bo.price,
        quantity: bo.quantity,
        nameRoom: bo.nameRoom,
        address: bo.address,
        category: bo.category,
        totalPrice: t
    }
    listOrder.push(a)
    let data = {
        ...req.body,
        timeOrder: date,
        orderRoom: listOrder,
        sum: order.sum + t
    }
    const orders = await Oder.findByIdAndUpdate({ _id: req.params.id }, data, { new: true });
    res.status(200).json({
        status: true
    })
}

exports.deleteItemOrder = async (req, res) => {
    let d = new Date;
    let date = d.getHours() + ':' + d.getMinutes() + '  ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
    let listData = []
    const order = await Oder.findById(req.params.idOrder);
    let sum = order.sum
    let listOrder = order.orderRoom
    for (let index = 0; index < listOrder.length; index++) {
        const element = listOrder[index];
        if (!(element._id==req.params.idItem)) {
            listData.push(element);
            sum -=element.totalPrice
        }  
    }
    let data = {
        ...req.body,
        timeOrder: date,
        orderRoom: listData,
        sum: sum
    }
    const orders = await Oder.findByIdAndUpdate({ _id: req.params.idOrder }, data, { new: true });
    res.status(200).json({
        status: true
    })
}