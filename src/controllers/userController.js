const User = require('../models/user');
const Order = require('../models/order')

exports.getAllUser = async (req, res) => {
    const user = await User.find();
    res.status(200).json({ ListUser: user });
};
exports.getUserById = async (req, res) => {
    const user = await User.findById(req.params.id);
    res.status(200).json({ user });
};
exports.createUser = async (req, res) => {
    const act = await User.create(req.body);
    res.status(200).json({
        act
    });
};
exports.deleteUser = async (req, res) => {
    const act = await User.findByIdAndDelete(req.params.id);
    const or = await Order.find({ idUser: req.params.id })
    for (let index = 0; index < or.length; index++) {
        const element = or[index];
        const t = await Order.findByIdAndDelete(element._id)
    }
    res.status(200).json({
        status: true,
    });
};

exports.updateUser = async (req, res) => {
    const act = await User.findByIdAndDelete({ _id: req.params.is }, req.body, { new: true });
    res.status(200).json({
        status: true,
    });
}
exports.PaginationUser = async (req, res) => {
    const option = {
        page: req.params.activePage,
        limit: 10,
    }
    const pro = await User.paginate({}, option);
    res.status(200).json({
        data: pro
    })
};
exports.login = async (req, res) => {
    console.log(req.body);
    const user = await User.findOne({ email: req.body.email, password: req.body.password });
    console.log(user);
    let check = false;
    let datas = null;
    if (user != null) {
        check = true;
        datas = user;
    }
    res.json({
        status: check,
        data: datas
    })
}