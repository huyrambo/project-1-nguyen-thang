const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        name: { type: String, required: true },
        email: {
            type: String, required: true, unique: true, index: true, dropDups: true,
        },
        password: { type: String, required: true },
    }
)
userSchema.plugin(mongoosePaginate)
module.exports = mongoose.model("User", userSchema);