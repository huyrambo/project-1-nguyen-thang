const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const roomOderSchema = new Schema({
    nameRoom: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    address: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true,
        default: 0
    },
    totalPrice: {
        type: Number,
        required: true,
        default: 0
    }


})
const oderSchema = new Schema({
    idUser: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    orderRoom: [roomOderSchema],
    timeOrder: {
        type: String,
        required: true,
    },
    sum: {
        type: Number,
        required: true,
        default: 0
    }
})
oderSchema.plugin(mongoosePaginate)
module.exports = mongoose.model("Oder", oderSchema)
